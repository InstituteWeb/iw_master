<?php
namespace VendorName\Project;

/*  | This script is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Environmental\Environment;

/**
 * Production Environment
 *
 * @param Environment $environment
 * @return callable function
 */
return function (Environment $environment) {
    // Environment settings
//    $environment->setDatabaseCredentials(
//        'username',
//        getenv('TYPO3_DB_PASSWORD'),
//        'db',
//        '127.0.0.1'
//    );

    $environment->typo3ConfVars['SYS']['exceptionalErrors'] = E_USER_ERROR | E_RECOVERABLE_ERROR;
    $environment->typo3ConfVars['SYS']['systemLogLevel'] = 2;
    $environment->typo3ConfVars['SYS']['systemLog'] = 'error_log';

    $environment->typo3ConfVars['MAIL']['transport'] = 'smtp';
    // TODO: add missing smtp default settings
};
