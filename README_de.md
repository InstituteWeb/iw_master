# TYPO3 Distribution: iw_master

iw_master ist eine Distribution für Enterprise-TYPO3-CMS-Umgebungen - einfach deploybar, hoch skalierbar.

Sie basiert auf Helmut Hummels [TYPO3 Distribution](https://github.com/helhum/TYPO3-Distribution) und vielen seiner tollen
Pakete und TYPO3 Erweiterungen.


## Erste Schritte

Nach der Installation (siehe unten), sollte beim Aufruf des Frontends ein rudimentäres Webseiten-Konstrukt erscheinen.

Wo fangen wir an, um all dies anzupassen?

In **web/assets/** können alle Assets (z.B. Bilder, Fonts) sowie Stylesheets und JavaScripte abgelegt werden. 
TYPO3 muss allerdings noch mitgeteilt werden, welche Assets (css/js) wie eingebunden werden sollen. Das passiert in der
projektspezifischen Erweiterung EXT:project. Sie befindet sich in **web/typo3conf/ext/project/**

Das Einbinden der Assets wird in der Datei *EXT:project/TypoScript/Setup/AssetIncludes.ts* gesteuert. In der nachfolgenden
Readme finden sich zu jeder TypoScript-Datei, die mit iw_master ausgeliefert wird, inhaltliche Beschreibungen. 

Die Fluid-Templates kontrollieren die HTML-Ausgabe von TYPO3 und liegen ebenfalls in EXT:project.
Sie sind aufgeteilt nach: 

- Backend Layouts
- Content Elemente (fluid_styled_content)
- Extensions (like news, gridelements, and so many more... :))

Mehr Infos über Backend-Layouts und Gridelements findet man in der Sektion "Wo finde ich was".


## Roadmap

Zwei essentielle Features fehlen, um hieraus eine vollständige Enterprise TYPO3 Distribtuion zu machen:
Der **Frontend Master** und die **Deployment Skripte**.
 
Der **Frontend Master** wird hauptsächlich im **/markup** liegen, Grunt-, Gulp- und Npm-Dateien im Projekt-Root. HTML-Templates 
werden die Fluid Template Engine nutzen (Standlone, separat vom TYPO3 Kern, gesteuert mit Grunt bzw. Gulp). 

Es ist noch nicht final entschieden, welches Tool für das **automatische Deployment** genommen wird, aber PHP Deployer
ist in der engeren Auswahl.


## Installation

Um ein neues Projekt zu erstellen, das auf der iw_master-Distribution basiert, kann PHP Composer verwenden werden: 

``` 
composer create-project instituteweb/iw_master:"^3.0" projectfolder
```

Dies macht:

- Es wird ein neuer Ordner "projectfolder" erstellt
- Alle Dateien vom iw_master und aller Abhängigkeiten (Biblioteken und TYPO3 Erweiterungen) werden heruntergeladen 
- Die sogenannten "Binaries" in den /bin Ordner installiert. Binaries können ausgeführt werden und erfüllen Aufgaben.
- Ruft das iw_master Willkommens-Skript auf, welches auf das Installations-Skript für TYPO3 (`bin/typo3cms`) weiterleitet.

Nach einem `composer update` werden die Schemas in der Datenbank aktualisiert (neue Tabellen/Felder werden erstellt oder 
umbenannt). Nach einem `composer dump-autoload` wird die PackageStates.php generiert. Daher sollte sie nicht zur
Versionierung hinzugefügt werden. Außerdem werden alle TYPO3 Verzeichnisse geprüft und erstellt.

**Wichtig:** Die VHost-Konfiguration des Webservers muss auf das **/web**-Verzeichnis zeigen. Dies ist das einzige
öffentliche Verzeichnis. Alle anderen Dateien sind strikt vor dem öffentlichen Zugriff geschützt. Dies
härtet das System.


### TYPO3-(System-)Erweiterungen und Composer

Alle TYPO3 Erweiterungen, die in der composer.json in der *require*- und *require-dev*-Sektion stehen, werden 
**automatisch geladen** und **aktiviert**. Dies geht ebenfalls mit TYPO3-System-Erweiterungen:

```
"require": {
    // ...
    "typo3/cms-fluid-styled-content": "^7.6",
}
```

Es handelt sich hierbei um den Extension-Key, mit Vorangestelltem "typo3/cms-" und umgewandelten Unterstrich 
(_) zu Minus (-), wie im Beispiel darüber für EXT:fluid_styled_content. 


### Eigene Erweiterungen und Composer

Wenn **eigene Erweiterungen eingebunden werden sollen** müssen zwei Dinge in der composer.json angepasst werden:

Die PHP Klassen müssen an den Vendor-Projekt-Namen gebunden werden. Beispiel für die Erweiterung EXT:project:
```
"autoload": {
    "psr-4": {
        "VendorName\\Project\\": "web/typo3conf/ext/project"
    }
},
```

Um die Extension zu aktivieren, kann typo3_console verwendet werden. Dazu muss folgendes in die Kommandozeile 
eingegeben werden:
```
bin/typo3cms extension:activate project custom_extension another_one
```

Zusätzlich sollte die Erweiterung in der composer.json ergänzt werden - für den Fall, dass man sie immer (auch im Produktivsystem)
aktiviert haben möchte.


#### Skripte

iw_master liefert zwei Composer-Events, welche helfen können:

##### \\InstituteWeb\\Environmental\\Scripts\\ComposerEvents::stopIfNoDatabaseConnection

Dies überprüft, ob das aktuelle Setup (d.h. die Umgebung) über eine gültige Datenbankverbindung verfügt. Wenn dem nicht so ist, werden 
alle nachfolgenden Scripte nicht mehr ausgeführt. Dies ist hilfreich um Fehlern vorzubeugen, die durch Scripte 
entstehen, die eine Datenbank benötigen, um funktionieren zu können.

##### \\InstituteWeb\\ComposerScripts\\ImprovedScriptExecution::apply

Wenn die [ImprovedScriptExecution](https://bitbucket.org/InstituteWeb/composer-scripts) "applied" (angewendet) wird,
erhält man zwei Dinge:

1. Es ist möglich `@php` zu nutzen als Skript (dies wurde zu Composer direkt hinzugefpgt, [Danke Jordi](https://github.com/composer/composer/issues/5957#issuecomment-266287653)). 
   Hierdurch wird die PHP_BINARY mit der schon zuvor Composer erfolgreich aufgerufen wurde auch verwendet um die Skripte
   auszuführen. Hierdurch können die selben Skriptaufrufe in composer.json verwendet werden, auch wenn unterschiedliche
   Umgebungen, unterschiedliche PHP-Binaries erfordern (z.B. `php_cli`).
2. Nur für Windows: Zusätzlich aktualisiert er den Zeilentrenner (directory separator) auf Windows und **extrahiert das PHP-Skript aus der Batch-Datei**
   und ersetzt dieses mit dem .bat-Datei-Aufruf. Hierdurch können wir die selben Skriptaufrufe **systemunabhängig** verwenden.


## Wo finde ich was

### EXT:project

Die Erweiterung EXT:project befindet sich in `/web/typo3conf/ext/project/`. Sie inhaltet alle projektspezifischen Inhalte
und ist auf jedem System anders. Die Basisstruktur von _iw_master_ hilft, die Orientierung zu behalten und sich schnell
zurecht zu finden.


### Backend Layouts

Die PageTS-Konfiguration und die Fluid-Templates liegen unter:

```
EXT:configuration/Templates/BackendLayouts/
```

`Layout`- und `Partial`-Ordner befinden sich im Root von EXT:configuration. Hier können neue Backend Layouts hinzugefügt 
werden. Diese müssen aber zusätzlich in `EXT:configuration/TypoScript/Setup/Templates.ts` registriert werden.


### Gridelements

Gridelements sind als Pakete verpackt. Jedes Paket beinhaltet das Icon, eine setup.ts- und eine tsconfig.t3-Datei.
Die unterschiedlichen Dateiendungen sind hier wichtig, um zwischen Setup (ts) und PageTS (t3) zu differenzieren.

Die Pakete liegen unter:

```
EXT:configuration/Templates/Extensions/Gridelements/
```


### TypoScript Configurations

In `EXT:configuration/TypoScript/` sind alle TypoScript-Dateien organisiert. Die folgenden drei Dateien müssen in 
*sys_template* und der *root page* eingebunden werden:

- `EXT:configuration/TypoScript/Constants.ts`
- `EXT:configuration/TypoScript/Setup.ts`
- `EXT:configuration/TypoScript/TsConfig.ts`

Hier können auch UserTsConfig Dateien abgelegt werden.


#### Constants.ts

In der Datei `constants.ts` sind alle Konstanten konfiguriert. Wer besonders viele Konstanten konfigurieren möchte, kann
die Datei je nach Bedarf selbstständig aufteilen. Normalerweise ist das aber nicht von Nöten. Dabei werden im iw_master 
standardmäßig **lokale** Konstanten genutzt, da das Paket mehrere Seitenbäume berücksichtigen möchte, in denen Daten wie
*fe_users* zwar zusammengenutzt, die Konstanten aber aufgeteilt werden sollen. Wer die Konstanten global konfigurieren
möchte, sollte dementsprechend **globale** Konstanten nutzen.

Darüber hinaus gibt es eine weitere Konstante für **Übersetzungen**, die auf die Datei locallang.xlf zeigt. 

Das erlaubt uns später, dieses beispielhafte TypoScript zu nutzen:

```
10 = TEXT
10.data = {$translate}keyInLocallangFile
```


#### TsConfig.ts

In der Datei `TsConfig.ts` werden alle Dateien aus dem TsConfig-Ordner geladen. Wir haben versucht, das PageTS nach
Einsatzfeldern (wie z.B. dem RTE oder den TCA-Änderungen in TCE-Form) zu unterteilen. Auch Erweiterungen (wie z.B. News)
können eigene Dateien haben, sofern das für sinnvoll erachtet wird.


#### Config.ts

Im Setup-Ordner finden wir das grundsätzliche TypoScript. Dabei wird als erstes die Datei Config.ts eingebunden, in der 
alle Optionen des TypoScript-Objekts **config** konfiguriert werden.

Es handelt sich hierbei um eine Basiskonfiguration (mit aktivierter Kompression und HTML5-Deklaration), die je nach Bedarf
beliebig skalierbar ist.
Wenn der Backend-Benutzer angemeldet ist, ist die Variable config.no_cache aktiviert.


#### Page.ts

Die Datei `Page.ts` ist die zweite wichtige Konfigurationsdatei. Sie beinhaltet das TypoScript-Objekt **page** und konfiguriert
neben den Meta-Tags und dem Viewport auch Head-Daten wie individuelle Seitentitel, denen aus der Übersetzung beispielsweise 
automatisiert Zeichenketten (wie der Name eines Unternehmens) angehangen werden können. Darüber hinaus sorgt die Datei dafür,
dass automatisch ein Canonical-Tag hinzugefügt wird, wenn das `content_from_pid`-Feature in den Seiteneigenschaften aktiviert
ist, sowie dafür, dass die aktuelle Sprachen-ID zum Body-Tag hinzugefügt wird. 


#### Templates.ts

Die Datei `Templates.ts` beinhaltet den dritten großen Bereich der Konfiguration und wird in der Page.ts genutzt, wenn wir 
dem TypoScript-Objekt **page** das Attribut `10 =< lib.templates.base` übergeben. Dabei handelt es sich um eine Referenz, sodass
Änderungen an `lib.templates.base` später ebenfalls einen Einfluss im TypoScript haben.

In der Datei werden die Pfade zu den Partial- und Layout-Ordnern gesetzt und die Variablen definiert. Dabei versuchen wir, den
cObject-Viewhelper soweit es geht zu vermeiden, da dies eine Abhängigkeit innerhalb des Templates verursacht. Besser ist es,
alle Informationen von Beginn an im Rendering zu übergeben. Wenn Variablen nicht ausreichen sollten, können die Data Prozessoren
von Fluid allerdings weiterhin genutzt werden.

Außerdem definieren wir hier den Pfad zum Template. Dabei wird das Template in Abhängigkeit von einem ausgewählten Backend-Layout
gesetzt. Diese Lösung resultiert aus den Erfahrungen mit Best-Practice-Beispielen. Wer eine andere Lösung bevorzugt, kann die 
Abhängigkeit allerdings zu jeder Zeit lösen.

Schließlich stellt der iw_master die Möglichkeit des Hinzufügens von Spalten bereit, die in **lib.templates.columns** konfiguriert
werden können. Wer ein paar Spalten mehr benötigt, kann die an dieser zentralen Stelle definieren und an die Variablen übergeben.


#### AssetIncludes.ts

Die Datei `AssetIncludes.ts` fügt die Assets hinzu, die im Ordner `/web/assets/` lagern. Wer Task runner wie Grunt oder Gulp nutzt,
kann den Pfad zum Output hierhin setzen.

Übrigens: alle Assets werden automatisch komprimiert, minifiziert und verknüpft. Das kann für jede Datei einzeln oder global in 
der Datei `Config.ts` deaktiviert werden.


#### Languages.ts

Die Datei `Languages.ts` beinhaltet eine beispielhafte Konfiguration für ein zweisprachiges System - mit Deutsch als Standard- und
Englisch als Übersetzungssprache.


#### TtContent.ts

Die Datei `TtContent.ts` beinhaltet alle Anpassungen von **tt_content** (css_styled_content or **fluid_styled_content (default)**).



#### Debug.ts

Die Datei `Debug.ts` sorgt dafür, dass im Frontend bestimmte Debug-Informationen angezeigt werden. Fügt man den String `?debug=1` 
ans Ende der Adresszeile hinzu, sind alle Debug-Informationen von Typo3 aktiviert und die Kompression deaktiviert. Zudem werden 
einige Debug-Informationen als Kommentare im HTML-Code ausgegeben. Wer CSS- und JS-Datei getrennt voneinander debuggen möchte, kann
`?cssdebug=1` (für CSS) bzw. `?jsdebug=1` (für JS) nutzen. Wer den Admin-Panel aktivieren möchte, kann `?adminpanel=1` nutzen.

Schließlich sorgt die Datei dafür, dass im Frontend ein orangener (Development) bzw. gelber (Testing) Balken in der rechten unteren 
Ecke zu sehen ist, um dem Benutzer eine zusätzliche Orientierungsmöglichkeit zu schaffen, ob er sich auf der Dev- oder Test-Umgebung
aufhält.
