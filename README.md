# TYPO3 Distribution: iw_master

iw_master is a distribution for TYPO3 CMS for enterprise requirements. Easy deployable. Highly scalable.

It's based on Helmut Hummel's [TYPO3 distribution](https://github.com/helhum/TYPO3-Distribution) and many of his great
packages and TYPO3 extensions.


## First steps

After installation (see below), you should see a rudimentary website construct.

So how to update all this, where to start?

In **web/assets/** you can place all your stylesheets, javascript and assets, like images or fonts. You need to tell TYPO3
to load your assets (css/js). You do this in the project specific extension called "project" (EXT:project).
It's located in **web/typo3conf/ext/project/**

The inclusion of assets is stored in *EXT:project/TypoScript/Setup/AssetIncludes.ts*. Below in readme you find a
description for each base typoscript file shipped with iw_master.

The fluid templates to control the HTML output of TYPO3 are also stored in EXT:project. They are split to:

- Backend Layouts
- Content Elements (fluid_styled_content)
- Extensions (like news, gridelements, and so much more... :))

Read more about Backend Layouts and Gridelements in "Where you find" section.


## Roadmap

Two essential features are missing to make this a complete enterprise TYPO3 distribution.
The **frontend master** and the **deployment script**.
 
**Frontend master** will be primarily located in **/markup** folder. Grunt/Gulp/Npm files in root. HTML templates will 
use fluid template engine (standalone, separated from TYPO3 core, triggered with Grunt/Gulp).
 
It is not finally decided which tool to choose for **automated deployment**, but PHP Deployer is in the inner decision 
circle.


## Installation

To create a new TYPO3 project based on iw_master, just use composer like this:

``` 
composer create-project instituteweb/iw_master:"^3.0" projectfolder
```

What it does:

- Create new folder "projectfolder"
- Adds all files from iw_master and fetches dependencies (libraries and TYPO3 extensions)
- Installs binaries of dependencies to /bin folder
- Calls iw_master welcome script which leads you to TYPO3 installation (`bin/config install`)

After `composer update` it updates the database schema (creates new tables/fields or renames them). After
`composer dump-autoload` it generates PackageStates.php (which is not necessary to add to vcs). And it creates all
TYPO3 folders. 

**Important:** Webserver's vhost configuration need to point to **/web** directory. This is the only public directory.
All other files are strictly excluded from public access this way, which hardens your system.


### TYPO3 (sys-)extensions and composer

All TYPO3 extensions, defined in *require* and *require-dev* section in composer.json are **loaded** and **activated
automatically**. You can also activate system extensions this way:

```
"require": {
    // ...
    "typo3/cms-fluid-styled-content": "^7.6",
}
```

It's the sys-extension key prefixed with "typo3/cms-" and underscores (_) are converted to dash (-). 
Like in example above for EXT:fluid_styled_content.

### Own extensions and composer

In case you provide **your own extension** you need to do two things in composer.json:

Map paths of php classes to your namespace. Example "extension project":
```
"autoload": {
    "psr-4": {
        "VendorName\\Project\\": "web/typo3conf/ext/project"
    }
},
```

Use typo3_console to activate your extension. Enter this on command line:
```
bin/typo3cms extension:activate project custom_extension another_one
```

You should also add the extension to composer.json, in case you always (also on production) want to enable the extension
automatically.


#### Scripts

iw_master provides two composer events, which may help you:

##### \\InstituteWeb\\Environmental\\Scripts\\ComposerEvents::stopIfNoDatabaseConnection

This checks if the current setup/environment has a valid database connection. If not, all scripts behind are not
executed anymore. Helpful to avoid errors from tasks which require a valid database connection.

##### \\InstituteWeb\\ComposerScripts\\ImprovedScriptExecution::apply

When you apply the [ImprovedScriptExecution](https://bitbucket.org/InstituteWeb/composer-scripts) you get two things:

1. You can use `@php` as script (which has been added to composer natively, [thanks, Jordi](https://github.com/composer/composer/issues/5957#issuecomment-266287653)). 
   This uses the PHP_BINARY of the composer call itself and makes it possible to use the same script calls also on 
   systems with different php binary names (like e.g. `php_cli`) 
2. For Windows only: It also updates the directory separators on Windows and **extracts the php script from batch file**
   and replaces it with .bat-file call. This allows you to run the same script calls **system independent**.


## Where you find

### EXT:project

The extension "project" is located in `/web/typo3conf/ext/project/`. It contains all project related stuff and is on
every system different. The base structure of iw_master helps to orient and find stuff quickly.  


### Backend Layouts

PageTS configuration and fluid template:

```
EXT:configuration/Templates/BackendLayouts/
```

`Layout` and `Partial` folder are located in the root of EXT:configuration. If you want to add new backend layouts, you
need to register them in `EXT:configuration/TypoScript/Setup/Templates.ts`.


### Gridelements

Gridelements are stored as packages. Each package contains an icon, setup.ts and tsconfig.t3.
The different file extension is important here, to differ between Setup (ts) and PageTS (t3) files.

The package are located here:

```
EXT:configuration/Templates/Extensions/Gridelements/
```


### TypoScript Configurations

In `EXT:configuration/TypoScript/` all TypoScript files are organized. You need to include these files to your
*sys_template* and *root page*:

- `EXT:configuration/TypoScript/Constants.ts`
- `EXT:configuration/TypoScript/Setup.ts`
- `EXT:configuration/TypoScript/TsConfig.ts`

If you want to store UserTsConfig to files, you could add the file here.


#### Constants.ts

All constants are located in this single file. You can split it up if you need but usually, this is not necessary. In
constants.ts we use the **local** primarily. We assume that you could have several page trees which share data, like
*fe_users*. So local constants are for the current page tree. A globally available constant may use **global**. 

We also have a **translate** constant, which points to the locallang.xlf for translations.

This allows us to use this in TypoScript later:

```
10 = TEXT
10.data = {$translate}keyInLocallangFile
```


#### TsConfig.ts

It loads all files in TsConfig folder. We try to split the PageTS to operation fields. Like RTE or all TCA 
manipulations in Tceform. Also, extensions may have their own file here. Use it as wise as possible.


#### Config.ts

In Setup folder, we find the main typoscript. The very first loaded one is Config.ts, which contains all configuration
of TypoScript's top level object **config**.

It's a solid basic configuration, with enabled compression, html5 preset.
When backend user is logged in `config.no_cache` is true.


#### Page.ts

Page.ts is the second important configuration file. It provides our **page** object and controls meta-tags, viewport and
headerData stuff like a custom page title, which can get suffixed by locallang automatically. Also, it adds automatically
a canonical tag if you used `content_from_pid` feature in page options and it appends the current language uid to
body tag classes and data attribute.


#### Templates.ts

You already guessed it, the Templates.ts is the third major part in this configuration. It is used in Page.ts when
we told our **page** to `10 =< lib.templates.base`. It's a reference, so changes to `lib.templates.base` will also take
effect, when you change them later, in TypoScript.

It defines the paths of partial and layout folder and got the variables defined. We try to avoid using the cObject 
viewhelper, because this creates a dependency from inside the template. It is better to pass all information to render
in view, from the very beginning. If variables are not enough, you can also use Fluid's data processors.

We also define the path of the template to render here. It's based on the selected backend layout. This is a best
practice solution. But you can decouple the selected backend layout from the displayed fluid template if you need it.

Also we have all available columns defined in **lib.templates.columns**. So if you have some extra columns, you can
define them at this central point, and pass them to variables, above.


#### AssetIncludes.ts

It includes the assets, which are stored in `/web/assets/`. If you use task runner like grunt or gulp you can put the
output to this location.

Good to know: All assets are compressed, minified and concatenated. You can change this for each asset or globally in
`Config.ts`.


#### Languages.ts

Contains example configuration of a two-language system, with German default and English as a foreign language.


#### TtContent.ts

Contains all modifications to **tt_content** (css_styled_content or **fluid_styled_content (default)**).



#### Debug.ts

When `?debug=1` is in query string, all debugging of TYPO3 is enabled, asset compression disabled, and it prints some
debug information in source code as HTML comment. You can also use `?cssdebug=1` and `?jsdebug=1` to
control it separately and `?adminpanel=1` to enable the admin panel.

Also, it displays an orange (Development) or yellow (Testing) badge in the bottom right corner to ensure the visitor to
be on a dev or test environment.
