<?php

/*  | This script is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Vieweg <armin@v.ieweg.de>
 */

/**
 * UrlConfiguration (RealURL)
 */
return [
    'encodeSpURL_postProc' => [],
    'decodeSpURL_preProc' => [],

    '_DEFAULT' => [
        'enableDomainLookup' => 1,
        'init' => [
            'enableCHashCache' => 1,
            'defaultToHTMLsuffixOnPrev' => true,
            'enableUrlDecodeCache' => 1,
            'enableUrlEncodeHash' => 1,
            'emptyUrlReturnValue' => '/',
            'postVarSet_failureMode' => '',
            'appendMissingSlash' => false
        ],
        'preVars' => [
            'L' => [
                'GETvar' => 'L',
                'valueMap' => [],
                'valueDefault' => '',
                'noMatch' => 'bypass',
            ],
            'type' => [
                'GETvar' => 'type',
                'valueMap' => [
                    'feed' => 100,
                ],
                'valueDefault' => 0,
                'noMatch' => 'bypass',
            ]
        ],
        'pagePath' => [
            'type' => 'user',
            'spaceCharacter' => '-',
            'languageGetVar' => 'L',
            'expireDays' => 3,
            'segTitleFieldList' => 'tx_realurl_pathsegment,alias,nav_title,title',
        ],
        'fileName' => [
            'defaultToHTMLsuffixOnPrev' => 1,
            'index' => [
//                'rss.xml' => array(
//                    'keyValues' => array(
//                        'type' => '100',
//                    ),
//                ),
            ],
        ],
        'fixedPostVarSets' => [],
        'postVarSets' => [
            '_DEFAULT' => [
                'tx_news' => [
                    [
                        'GETvar' => 'tx_news_pi1[controller]',
                        'noMatch' => 'bypass'
                    ],
                    [
                        'GETvar' => 'tx_news_pi1[action]',
                        'noMatch' => 'bypass'
                    ],
                    [
                        'GETvar' => 'tx_news_pi1[news]',
                        'lookUpTable' => [
                            'table' => 'tx_news_domain_model_news',
                            'id_field' => 'uid',
                            'alias_field' => 'title',
                            'addWhereClause' => ' AND NOT deleted',
                            'useUniqueCache' => 1,
                            'useUniqueCache_conf' => [
                                'strtolower' => 1,
                                'spaceCharacter' => '-',
                            ],
                            'languageGetVar' => 'L',
                            'languageExceptionUids' => '',
                            'languageField' => 'sys_language_uid',
                            'transOrigPointerField' => 'l10n_parent',
                            'autoUpdate' => 1,
                            'expireDays' => 180,
                        ],
                    ],
                ],
            ],
        ],
    ],
];
