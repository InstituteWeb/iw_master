# Navigation: Language Switch
lib.navigations.language = HMENU
lib.navigations.language {
	special = language
	special.value = {$local.languageLabels.availableSysLanguageUids}

	addQueryString = 1

	1 = TMENU
	1 {
		NO = 1
		NO {
			wrapItemAndSub = <li>|</li>
			stdWrap.htmlSpecialChars = 1
			stdWrap.cObject = TEXT
			stdWrap.cObject.value = {$local.languageLabels.availableLabels}
		}
		ACT < .NO
		ACT.wrapItemAndSub = <li class="current">|</li>
		USERDEF2 < .ACT
	}

	wrap = <ul class="languageNavigation"><li><a href="#">{$local.languageLabels.current}</a><ul>|</ul></li></ul>
}
