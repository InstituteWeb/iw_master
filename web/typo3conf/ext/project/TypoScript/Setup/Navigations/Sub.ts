# Navigation: Main navigation (2nd+ level)
lib.navigations.sub = HMENU
lib.navigations.sub {
	entryLevel = 1

	1 = TMENU
	1 {
		noBlur = 1
		expAll = 1

		NO = 1
		NO.wrapItemAndSub = <li class="first">|</li>|*|<li>|</li>|*|<li class="last">|</li>

		ACT < .NO
		ACT.wrapItemAndSub = <li class="first active">|</li>|*|<li class="active">|</li>|*|<li class="last active">|</li>

		wrap = <ul>|</ul>
	}

	2 < .1
	3 < .2
	4 < .3
	5 < .4
}
