# Navigation: Main navigation (1st level only)
lib.navigations.main = HMENU
lib.navigations.main {
	1 = TMENU
	1 {
		noBlur = 1
		expAll = 1

		NO = 1
		NO.wrapItemAndSub = <li class="first">|</li>|*|<li>|</li>|*|<li class="last">|</li>

		ACT < .NO
		ACT.wrapItemAndSub = <li class="first active">|</li>|*|<li class="active">|</li>|*|<li class="last active">|</li>

		wrap = <ul>|</ul>
	}
}
