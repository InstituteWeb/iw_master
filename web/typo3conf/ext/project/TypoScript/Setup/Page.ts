# Initiate page
page = PAGE
page {
	typeNum = 0
	10 =< lib.templates.base

	shortcutIcon = assets/images/favicon.ico

	# META Tag definitions
	meta {
		X-UA-Compatible = IE=edge
		X-UA-Compatible.httpEquivalent = 1

		description {
			field = description
			ifEmpty.data = {$translate}metaDescriptionFallback
		}
		keywords {
			field = keywords
			ifEmpty.data = {$translate}metaKeywordsFallback
		}
		author {
			field = author
		}
		copyright.data = {$translate}companyName


		language = de,en
		imagetoolbar = false
		viewport = width=device-width, initial-scale=1
	}

	# Header data definitions
	headerData {
		# Title Tag
		1450713315 = COA
		1450713315 {
			10 = TEXT
			10.field = title

			20 = TEXT
			20.data = {$translate}pageTitleSuffix
			20.noTrimWrap = | - ||

			wrap = <title>|</title>
		}

		# Optional canonical tag if content_from_pid is set
		1391075690 = TEXT
		1391075690 {
			typolink.parameter.field = content_from_pid
			typolink.returnLast = url
			typolink.forceAbsoluteUrl = 1
			wrap = <link rel="canonical" href="/|" />
			if.value = 0
			if.isGreaterThan.field = content_from_pid
		}
	}

	# Body Tag Rendering
	bodyTagCObject = COA
	bodyTagCObject {
		10 = TEXT
		10 {
			value = default
			stdWrap.noTrimWrap = |language-| |
		}
		stdWrap {
			trim = 1
			dataWrap = <body class="|" data-languid="{TSFE:sys_language_uid}">
		}
	}
}
