lib.contents.logo = COA
lib.contents.logo {
    10 = IMAGE
    10 {
        file = typo3/sysext/backend/Resources/Public/Images/typo3_black.svg
        file.width = 120
        altText.data = {$translate}companyName
        titleText.data = {$translate}companyName
        stdWrap.typolink.parameter = {$local.uids.home}
    }

    20 = TEXT
    20.data = {$translate}companySlogan
    20.wrap = <span class="slogan">|</span>
}
