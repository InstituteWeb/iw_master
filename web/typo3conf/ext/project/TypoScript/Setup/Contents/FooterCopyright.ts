lib.contents.footerCopyright = COA
lib.contents.footerCopyright {
    10 = LOAD_REGISTER
    10 {
        currentYear = TEXT
        currentYear.data = date : U
        currentYear.strftime = %Y
    }

    20 = TEXT
    20.value = &copy; {REGISTER:currentYear}
    20.insertData = 1

    30 = TEXT
    30.data = {$translate}footerCopyrightText
    30.noTrimWrap = | ||
}
