# Initiate template (used by page)
lib.templates.base = FLUIDTEMPLATE
lib.templates.base {
	partialRootPaths {
		10 = EXT:project/Partials/
	}
	layoutRootPaths {
		10 = EXT:project/Layouts/
	}

	variables {
		logo =< lib.contents.logo

		navMain =< lib.navigations.main
		navSub =< lib.navigations.sub
		navService =< lib.navigations.service
		navBreadcrumb =< lib.navigations.breadcrumb
		navLanguage =< lib.navigations.language

        # Column content (use eg. {columnMain -> f:format.raw()} for proper output in fluid template)
		columnLeft =< lib.templates.columns.left
		columnMain =< lib.templates.columns.main
		columnRight =< lib.templates.columns.right
		columnTop =< lib.templates.columns.top
		columnBottom =< lib.templates.columns.bottom

		footerCopyright =< lib.contents.footerCopyright
	}
}

# Choose template file (based on backend_layout, respecting heredity)
lib.templates.base.file.stdWrap.cObject = CASE
lib.templates.base.file.stdWrap.cObject {
	key.field = backend_layout
	key.ifEmpty.data = levelfield : -1 , backend_layout_next_level, slide

	default = TEXT
	default.value = EXT:project/Templates/BackendLayouts/Default.html

	pagets__Special = TEXT
	pagets__Special.value = EXT:project/Templates/BackendLayouts/Special.html
}

# Uncomment this when you use indexed_search or solr
# temp.typo3searchWrap = <!--TYPO3SEARCH_begin-->|<!--TYPO3SEARCH_end-->

# Column configuration
lib.templates.columns {
	left = COA
	left {
		20 < styles.content.get
		20.select.where = colPos=1
		20.wrap < temp.typo3searchWrap
	}

	main = COA
	main {
		20 < styles.content.get
		20.wrap < temp.typo3searchWrap
	}

	right = COA
	right {
		20 < styles.content.get
		20.select.where = colPos=2
		20.wrap < temp.typo3searchWrap
	}

	border = COA
	border {
		20 < styles.content.get
		20.select.where = colPos=3
	}

	top = COA
	top {
		20 < styles.content.get
		20.select.where = colPos=4
	}

	bottom = COA
	bottom {
		20 < styles.content.get
		20.select.where = colPos=5
	}
}
