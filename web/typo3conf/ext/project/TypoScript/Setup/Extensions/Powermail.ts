page {
	includeJSFooterlibs {
		powermailJQuery >
		powermailJQueryUi >
		powermailJQueryFormValidationLanguage >
		powermailJQueryFormValidation >
		powermailJQueryTabs >
		powermailJQueryUiDatepicker >
	}

	includeJSFooter {
		powermailForm >
	}

	includeCSS {
		powermailJQueryUiTheme >
		powermailJQueryUiDatepicker >
	}

	1000 >
}
