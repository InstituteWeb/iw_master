# -------------------------------------------------------------------------------------- BEGIN: Include static constants

<INCLUDE_TYPOSCRIPT: source="FILE:EXT:fluid_styled_content/Configuration/TypoScript/Static/constants.txt">

# ---------------------------------------------------------------------------------------- END: Include static constants
# ----------------------------------------------------------------------------------------------- BEGIN: Local constants

local {
    languageLabels {
        current = Deutsch
        availableSysLanguageUids = 0,1
        availableLabels = Deutsch||English
    }
}

[globalVar = GP:L = 1]
    local.languageLabels {
        current = English
        availableLabels = Deutsch||Englisch
    }
[global]

local.uids.home = 1
local.uids.navigation.service = 10

# ------------------------------------------------------------------------------------------------- END: Local constants
# ------------------------------------------------------------------------------------------------------ BEGIN: Settings

styles.templates.templateRootPath = EXT:project/Templates/ContentElements/
styles.templates.partialRootPath = EXT:project/Partials/
styles.templates.layoutRootPath = EXT:project/Layouts/

# -------------------------------------------------------------------------------------------------------- END: Settings
# -------------------------------------------------------------------------------------------- BEGIN: Translate shortcut

# Usage in typoscript:
# 10 = TEXT
# 10.data = {$translate}key
translate = LLL:EXT:project/Language/locallang.xlf:

# ---------------------------------------------------------------------------------------------- END: Translate shortcut
