# ------------------------------------------------------------------------------------------ BEGIN: Include static setup

<INCLUDE_TYPOSCRIPT: source="FILE:EXT:fluid_styled_content/Configuration/TypoScript/Static/setup.txt">

# -------------------------------------------------------------------------------------------- END: Include static setup
# ------------------------------------------------------------------------------------------------ BEGIN:  Include setup

<INCLUDE_TYPOSCRIPT: source="FILE:EXT:project/TypoScript/Setup/Config.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:project/TypoScript/Setup/Page.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:project/TypoScript/Setup/Templates.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:project/TypoScript/Setup/AssetIncludes.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:project/TypoScript/Setup/Languages.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:project/TypoScript/Setup/TtContent.ts">

<INCLUDE_TYPOSCRIPT: source="DIR:EXT:project/TypoScript/Setup/Extensions/" extensions="ts">
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:project/TypoScript/Setup/Navigations/" extensions="ts">
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:project/TypoScript/Setup/Contents/" extensions="ts">

<INCLUDE_TYPOSCRIPT: source="FILE:EXT:project/TypoScript/Setup/Debug.ts">

# --------------------------------------------------------------------------------------------------- END: Include setup
