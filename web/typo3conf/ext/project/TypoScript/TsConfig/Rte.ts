# RTE configuration
RTE {
	default {
		contentCSS = assets/css/rte.css
		ignoreMainStyleOverride = 1
		useCSS = 1
		showTagFreeClasses = 1

		showButtons (
			bold, italic, subscript, superscript, unorderedlist, outdent, indent, justifyfull, table, toggleborders, tableproperties,
			rowproperties, rowinsertabove, rowinsertunder, rowdelete, rowsplit, columninsertbefore, columninsertafter,
			columndelete, columnsplit, cellproperties, cellinsertbefore, cellinsertafter, celldelete, cellsplit, cellmerge,
			formatblock, insertcharacter, link, findreplace, chMode, removeformat, undo, redo, about, left, center, right, blockstyle
		)

		proc {
			entryHTMLparser_db {
				tags {
					ul.fixAttrib.class.set = list
				}
			}
			exitHTMLparser_db = 1
			exitHTMLparser_db {
				tags {
					b.remap = strong
					i.remap = em
				}
			}
		}
		disableAlignmentFieldsetInTableOperations = 0
		blindImageOptions = magic

		buttons {
			formatblock {
				removeItems = pre,address,article,aside,div,footer,header,h4,h5,h6,nav,blockquote,section
			}
		}
	}
}
