# TCEFORM configurations

TCEFORM.pages {
    # Disable field "layout" (we use backend_layouts instead)
    layout.disabled = 1

    # Disable option "none" for backend_layouts
    backend_layout.removeItems = -1
    backend_layout_next_level.removeItems = -1
}

TCEFORM.tt_content {
    #header_position.disabled = 1
    #date.disabled = 1
}
