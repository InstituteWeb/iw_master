tt_content.gridelements_pi1.20.10.setup.grid-50-50 {
	columns {
		20 {
			renderObj =< tt_content
			wrap = <div class="column left">|</div>
		}
		21 {
			renderObj =< tt_content
			wrap = <div class="column right">|</div>
		}
	}

	wrap = <div class="grid grid-50-50">|<div class="clear"></div></div>
	prepend < lib.stdheader
}
